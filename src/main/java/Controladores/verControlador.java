/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;


import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author rm
 */
@Controller
@RequestMapping("/ver.htm")
public class verControlador {
    @RequestMapping(method = RequestMethod.POST)
    public String ver(@RequestParam("txt1") String txt,Model m){
        
        if (txt.contains("1")) {
            m.addAttribute("var", "paso con uno");
            return "error";
        }
        return "OK";
    }
    
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String verGet(Model m){
        
        
        return "{\"success\":1}";
    }
    
    
    
    
}
